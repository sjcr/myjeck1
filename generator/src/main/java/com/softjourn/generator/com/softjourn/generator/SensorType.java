package com.softjourn.generator.com.softjourn.generator;

import javax.persistence.*;

/**
 * Created by vmoroz on 31.03.2016.
 */
@Entity
@Table(name="tbl_sensortype")
public class SensorType {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int sensorTypeID;
    private String sensorTypeName;
    private String sensorTypeDescription;

    public int getSensorTypeID() {
        return sensorTypeID;
    }

    public void setSensorTypeID(int sensorTypeID) {
        this.sensorTypeID = sensorTypeID;
    }

    public String getSensorTypeName() {
        return sensorTypeName;
    }

    public void setSensorTypeName(String sensorTypeName) {
        this.sensorTypeName = sensorTypeName;
    }

    public String getSensorTypeDescription() {
        return sensorTypeDescription;
    }

    public void setSensorTypeDescription(String sensorTypeDescription) {
        this.sensorTypeDescription = sensorTypeDescription;
    }
}
