package com.softjourn.generator.com.softjourn.generator.dao;

import com.softjourn.generator.com.softjourn.generator.HibernateUtil;
import com.softjourn.generator.com.softjourn.generator.Sensor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


/**
 * Created by vmoroz on 31.03.2016.
 */
public class SensorDAO {
    public Sensor getSensor(int sensorID){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Sensor sensor  = session.get(Sensor.class, sensorID);
        session.close();
        return sensor;
    }
    public void setSensor(Sensor sensor){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(sensor);
        session.getTransaction().commit();
        session.close();
    }
}
