package com.softjourn.generator.com.softjourn.generator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vmoroz on 31.03.2016.
 */

@Entity
@Table(name="tbl_sensor")
public class Sensor {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int sensorID;
    private int addressID;

    @ManyToOne(cascade = CascadeType.ALL)
    private SensorType sensorType;
    private Date date;
    private double value;

    public int getSensorID() {
        return sensorID;
    }

    public void setSensorID(int sensorID) {
        this.sensorID = sensorID;
    }
    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
